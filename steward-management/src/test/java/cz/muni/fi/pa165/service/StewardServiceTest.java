package cz.muni.fi.pa165.service;

import cz.muni.fi.pa165.data.model.Steward;
import cz.muni.fi.pa165.data.model.StewardShift;
import cz.muni.fi.pa165.data.repository.StewardRepository;
import cz.muni.fi.pa165.exceptions.EntityNotFoundException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
public class StewardServiceTest {

    @Mock
    private StewardRepository stewardRepository;

    @InjectMocks
    private StewardService stewardService;

    private int id = 1;

    Steward createTestSteward() {
        Steward steward = new Steward();
        steward.setFirstName("FName" + id);
        steward.setLastName("LName" + id);
        id++;
        return steward;
    }

    @Test
    void createSteward_creationSuccessful_returnsSteward() {
        // Arrange
        Steward steward = createTestSteward();
        Mockito.when(stewardRepository.createSteward(steward)).thenReturn(steward);

        // Act
        Steward returnedSteward = stewardService.createSteward(steward);

        // Assert
        assertThat(returnedSteward).isEqualTo(steward);
    }

    @Test
    void findById_stewardFound_returnsSteward() {
        // Arrange
        Steward steward = createTestSteward();
        Mockito.when(stewardRepository.findById(1L)).thenReturn(Optional.of(steward));

        // Act
        Steward returnedSteward = stewardService.findById(1L);

        // Assert
        assertThat(returnedSteward).isEqualTo(steward);
    }

    @Test
    void findById_stewardNotFound_throwsException() {
        // Arrange
        Mockito.when(stewardRepository.findById(1L)).thenReturn(Optional.empty());

        // Act
        assertThrows(EntityNotFoundException.class, () -> stewardService.findById(1L));

        // Assert
        Mockito.verify(stewardRepository, Mockito.times(1)).findById(1L);
    }

    @Test
    void getAllStewards_noStewardsListed_returnsEmptyListOfStewards() {
        // Arrange
        Mockito.when(stewardRepository.getAllStewards()).thenReturn(new ArrayList<>());

        // Act
        List<Steward> returnedStewards = stewardService.getAllStewards();

        // Assert
        assertThat(returnedStewards).isEmpty();
    }

    @Test
    void getAllStewards_someStewardsListed_returnsListOfStewards() {
        // Arrange
        Steward steward1 = createTestSteward();
        Steward steward2 = createTestSteward();
        List<Steward> stewards = List.of(steward1, steward2);
        Mockito.when(stewardRepository.getAllStewards()).thenReturn(stewards);

        // Act
        List<Steward> returnedStewards = stewardService.getAllStewards();

        // Assert
        assertThat(returnedStewards).isEqualTo(stewards);
    }

    @Test
    void updateSteward_stewardFound_returnsSteward() {
        // Arrange
        Steward steward = createTestSteward();
        Mockito.when(stewardRepository.updateSteward(steward)).thenReturn(Optional.of(steward));

        // Act
        Steward returnedSteward = stewardService.updateSteward(steward);

        // Assert
        assertThat(returnedSteward).isEqualTo(steward);
    }

    @Test
    void updateSteward_stewardNotFound_throwsException() {
        // Arrange
        Steward steward = createTestSteward();
        steward.setId(1L);
        Mockito.when(stewardRepository.updateSteward(steward)).thenReturn(Optional.empty());

        // Act
        assertThrows(EntityNotFoundException.class, () -> stewardService.updateSteward(steward));

        // Assert
        Mockito.verify(stewardRepository, Mockito.times(1)).updateSteward(steward);
    }

    @Test
    void updateSteward_stewardWithNoId_throwsException() {
        // Arrange
        Steward steward = createTestSteward();
        Mockito.when(stewardRepository.updateSteward(steward)).thenReturn(Optional.empty());

        // Act
        assertThrows(NullPointerException.class, () -> stewardService.updateSteward(steward));

        // Assert
        Mockito.verify(stewardRepository, Mockito.times(1)).updateSteward(steward);
    }

    @Test
    void deleteSteward_stewardFound_returnsSteward() {
        // Arrange
        Steward steward = createTestSteward();
        Mockito.when(stewardRepository.deleteSteward(1L)).thenReturn(Optional.of(steward));

        // Act
        Steward returnedSteward = stewardService.deleteSteward(1L);

        // Assert
        assertThat(returnedSteward).isEqualTo(steward);
    }

    @Test
    void deleteSteward_stewardNotFound_throwsException() {
        // Arrange
        Mockito.when(stewardRepository.deleteSteward(1L)).thenReturn(Optional.empty());

        // Act
        assertThrows(EntityNotFoundException.class, () -> stewardService.deleteSteward(1L));

        // Assert
        Mockito.verify(stewardRepository, Mockito.times(1)).deleteSteward(1L);
    }

    @Test
    void assignStewardToFlight_stewardFound_returnsSteward() {
        // Arrange
        StewardShift stewardShift = new StewardShift();
        Steward steward = createTestSteward();
        Mockito.when(stewardRepository.getStewardForFlight(stewardShift)).thenReturn(Optional.of(steward));

        // Act
        Steward returnedSteward = stewardService.assignStewardToFlight(stewardShift);

        // Assert
        assertThat(returnedSteward).isEqualTo(steward);
    }

    @Test
    void assignStewardToFlight_stewardNotFound_throwsException() {
        // Arrange
        StewardShift stewardShift = new StewardShift();
        Mockito.when(stewardRepository.getStewardForFlight(stewardShift)).thenReturn(Optional.empty());

        // Act
        assertThrows(EntityNotFoundException.class, () -> stewardService.assignStewardToFlight(stewardShift));

        // Assert
        Mockito.verify(stewardRepository, Mockito.times(1)).getStewardForFlight(stewardShift);
    }

}
