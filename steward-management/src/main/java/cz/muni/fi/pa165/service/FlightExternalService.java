package cz.muni.fi.pa165.service;

import cz.muni.fi.pa165.data.dto.FlightDto;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClient;

@Service
public class FlightExternalService implements FlightExternalServiceInterface {
    private static final String FLIGHT_MANAGEMENT_ADDRESS = "http://localhost:8081/";
    private static final String FLIGHT_PATH = "flights";
    private static final String ADD_STEWARD_PATH = "steward";

    @Override
    public FlightDto getFlightById(Long flightId) {
        String uri = FLIGHT_MANAGEMENT_ADDRESS + FLIGHT_PATH + '/' + flightId;
        RestClient restClient = RestClient.create();
        ResponseEntity<FlightDto> flightDtoResponseEntity = restClient.get()
                .uri(uri)
                .retrieve()
                .toEntity(FlightDto.class);
        FlightDto flightDto = flightDtoResponseEntity.getBody();
        System.out.println(flightDto);
        return flightDto;
    }

    @Override
    public FlightDto sendAssignedSteward(Long stewardId, Long flightId) {
        String uri = FLIGHT_MANAGEMENT_ADDRESS + FLIGHT_PATH + '/' + ADD_STEWARD_PATH;

        RestClient restClient = RestClient.builder()
                .baseUrl(uri)
                .build();

        ResponseEntity<FlightDto> flightDtoResponseEntity = restClient.put()
                .uri(uriBuilder -> uriBuilder
                        .queryParam("stewardId", stewardId)
                        .queryParam("flightId", flightId)
                        .build()
                )
                .retrieve()
                .toEntity(FlightDto.class);

        FlightDto flightDto = flightDtoResponseEntity.getBody();
        System.out.println(flightDto);
        return flightDto;
    }
}
