package cz.muni.fi.pa165.data.repository;

import cz.muni.fi.pa165.data.model.Steward;
import cz.muni.fi.pa165.data.model.StewardShift;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface StewardRepository {
    Steward createSteward(Steward stewardToSave);
    Optional<Steward> findById(Long id);
    List<Steward> getAllStewards();
    Optional<Steward> updateSteward(Steward stewardUpdated);
    Optional<Steward> deleteSteward(Long id);

    Optional<Steward> getStewardForFlight(StewardShift shift);
}
