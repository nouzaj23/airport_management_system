package cz.muni.fi.pa165;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StewardMicroservice {
    public static void main(String[] args) {
        SpringApplication.run(StewardMicroservice.class, args);
    }
}