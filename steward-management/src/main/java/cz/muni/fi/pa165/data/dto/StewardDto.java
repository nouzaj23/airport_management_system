package cz.muni.fi.pa165.data.dto;

import cz.muni.fi.pa165.data.model.StewardShift;
import io.swagger.v3.oas.annotations.media.Schema;

import java.util.List;

public class StewardDto {
    @Schema(description = "Automatically generated identifier")
    private Long id;
    @Schema(description = "Steward's first name", example = "John")
    private String firstName;
    @Schema(description = "Steward's last name", example = "Doe")
    private String lastName;

    private List<StewardShift> assignedShifts;

    public List<StewardShift> getAssignedShifts() {
        return assignedShifts;
    }

    public void setAssignedShifts(List<StewardShift> assignedShifts) {
        this.assignedShifts = assignedShifts;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
