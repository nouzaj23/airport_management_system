package cz.muni.fi.pa165.exceptions;

public class RestExceptionResponse {
    private String message;

    public RestExceptionResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return this.message;
    }
}
