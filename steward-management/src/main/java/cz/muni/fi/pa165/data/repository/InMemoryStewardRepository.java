package cz.muni.fi.pa165.data.repository;

import cz.muni.fi.pa165.data.model.Steward;
import cz.muni.fi.pa165.data.model.StewardShift;
import org.springframework.stereotype.Repository;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Repository
public class InMemoryStewardRepository implements StewardRepository{
    Map<Long, Steward> stewards;
    Long stewardId = 1L;

    public InMemoryStewardRepository() {
        this.stewards = new HashMap<>();
    }

    @Override
    public Steward createSteward(Steward stewardToSave) {
        stewardToSave.setId(stewardId);
        stewards.put(stewardId, stewardToSave);

        stewardId++;
        return stewardToSave;
    }

    @Override
    public Optional<Steward> findById(Long id) {
        return Optional.ofNullable(stewards.get(id));
    }

    @Override
    public List<Steward> getAllStewards() {
        return new ArrayList<>(stewards.values());
    }

    @Override
    public Optional<Steward> updateSteward(Steward stewardUpdated) {
        Steward originalSteward = stewards.replace(stewardUpdated.getId(), stewardUpdated);
        return findById(stewardUpdated.getId());
    }

    @Override
    public Optional<Steward> deleteSteward(Long id) {
        return Optional.ofNullable(stewards.remove(id));
    }

    @Override
    public Optional<Steward> getStewardForFlight(StewardShift shift) {
        OffsetDateTime departure2 = shift.getDepartureTime();
        OffsetDateTime arrival2 = shift.getArrivalTime();
        for (Steward steward : stewards.values()) {
            if (steward.getAssignedShifts().isEmpty()) {
                steward.assignShift(shift);
                return Optional.of(steward);
            }
            else {
                for (StewardShift stewardFlight : steward.getAssignedShifts()) {
                    OffsetDateTime departure1 = stewardFlight.getDepartureTime();
                    OffsetDateTime arrival1 = stewardFlight.getArrivalTime();
                    if (isNotOverlapping(departure1, arrival1, departure2, arrival2)) {
                        steward.assignShift(shift);
                        return Optional.of(steward);
                    }
                }
            }
        }
        return Optional.empty();
    }

    public static boolean isNotOverlapping(OffsetDateTime departure1, OffsetDateTime arrival1,
                                        OffsetDateTime departure2, OffsetDateTime arrival2) {
        return arrival2.isBefore(departure1) || departure2.isAfter(arrival1);
    }

}
