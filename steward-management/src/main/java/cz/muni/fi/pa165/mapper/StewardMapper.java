package cz.muni.fi.pa165.mapper;

import cz.muni.fi.pa165.data.dto.CreateStewardDto;
import cz.muni.fi.pa165.data.dto.FlightDto;
import cz.muni.fi.pa165.data.dto.StewardDto;
import cz.muni.fi.pa165.data.model.Steward;
import cz.muni.fi.pa165.data.model.StewardShift;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Mapper(componentModel = "spring")
public interface StewardMapper {
    @Mapping(target = "flightId", source = "id")
    StewardShift mapToEntity(FlightDto flightDto);
    StewardDto mapToDto(Steward steward);

    Steward mapToEntity(StewardDto stewardDto);

    Steward mapToEntity(CreateStewardDto stewardDto);

    List<StewardDto> mapToList(List<Steward> airplanes);
}
