package cz.muni.fi.pa165.service;

import cz.muni.fi.pa165.data.model.Steward;
import cz.muni.fi.pa165.data.model.StewardShift;
import cz.muni.fi.pa165.data.repository.StewardRepository;
import cz.muni.fi.pa165.exceptions.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class StewardService {
    private final StewardRepository stewardRepository;

    @Autowired
    public StewardService(StewardRepository stewardRepository) {
        this.stewardRepository = stewardRepository;
    }

    public Steward createSteward(Steward stewardToSave) {
        return stewardRepository.createSteward(stewardToSave);
    }

    public Steward findById(Long id) {
        Optional<Steward> stewardOptional = stewardRepository.findById(id);
        if (stewardOptional.isEmpty()) {
            throw new EntityNotFoundException(id);
        }
        return stewardOptional.get();
    }

    public List<Steward> getAllStewards() {
        return stewardRepository.getAllStewards();
    }

    public Steward updateSteward(Steward stewardUpdated) {
        Optional<Steward> stewardUpdatedOptional = stewardRepository.updateSteward(stewardUpdated);
        if (stewardUpdatedOptional.isEmpty()) {
            throw new EntityNotFoundException(stewardUpdated.getId());
        }
        return stewardUpdatedOptional.get();
    }

    public Steward deleteSteward(Long id) {
        Optional<Steward> stewardDeletedOptional = stewardRepository.deleteSteward(id);
        if (stewardDeletedOptional.isEmpty()) {
            throw new EntityNotFoundException(id);
        }
        return stewardDeletedOptional.get();
    }

    public Steward assignStewardToFlight(StewardShift shift) {
        Optional<Steward> stewardOptional = stewardRepository.getStewardForFlight(shift);
        if (stewardOptional.isEmpty()) {
            throw new EntityNotFoundException("Could not find steward for given flight");
        }
        return stewardOptional.get();
    }

}
