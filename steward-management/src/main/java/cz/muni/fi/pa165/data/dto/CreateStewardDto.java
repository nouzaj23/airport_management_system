package cz.muni.fi.pa165.data.dto;

import io.swagger.v3.oas.annotations.media.Schema;


public class CreateStewardDto {
    @Schema(description = "Steward's first name", example = "John")
    private String firstName;
    @Schema(description = "Steward's last name", example = "Doe")
    private String lastName;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

}
