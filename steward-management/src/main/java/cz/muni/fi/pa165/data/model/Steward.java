package cz.muni.fi.pa165.data.model;

import java.util.ArrayList;
import java.util.List;

public class Steward {
    private Long id;
    private String firstName;
    private String lastName;

    private List<StewardShift> assignedShifts = new ArrayList<>();

    public void assignShift(StewardShift shift) {
        this.assignedShifts.add(shift);
    }

    public List<StewardShift> getAssignedShifts() {
        return assignedShifts;
    }

    public void setAssignedShifts(List<StewardShift> assignedShifts) {
        this.assignedShifts = assignedShifts;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long uuid) {
        this.id = uuid;
    }
}
