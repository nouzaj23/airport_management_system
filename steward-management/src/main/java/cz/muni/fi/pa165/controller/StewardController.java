package cz.muni.fi.pa165.controller;

import cz.muni.fi.pa165.data.dto.CreateStewardDto;
import cz.muni.fi.pa165.data.dto.FlightDto;
import cz.muni.fi.pa165.data.dto.StewardDto;
import cz.muni.fi.pa165.exceptions.RestExceptionResponse;
import cz.muni.fi.pa165.facade.StewardFacade;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.servers.Server;
import io.swagger.v3.oas.annotations.servers.ServerVariable;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestClient;

import java.util.List;

@OpenAPIDefinition(
        info = @Info(
                title = "Steward Service",
                version = "0.1",
                description = """
                        Service for management of stewards.
                        Service contains CRUD methods for stewards
                        """
        ),
        servers = @Server(description = "localhost server",
                url = "{scheme}://{server}:{port}",
                variables = {
                        @ServerVariable(name = "scheme",
                                allowableValues = {"http", "https"},
                                defaultValue = "http"),
                        @ServerVariable(name = "server",
                                defaultValue = "localhost"),
                        @ServerVariable(name = "port",
                                defaultValue = "8080"),
                })
)
@Tag(name = "Steward", description = "Microservice for steward management")
@RestController
@RequestMapping(path = "/stewards", produces = MediaType.APPLICATION_JSON_VALUE)
public class StewardController {

    private final StewardFacade stewardFacade;

    @Autowired
    public StewardController(StewardFacade stewardFacade) {
        this.stewardFacade = stewardFacade;
    }


    @Operation(
            summary = "Create new steward",
            description = "Accepts object representing new steward which is then stored into repository",
            responses = {
                @ApiResponse(responseCode = "201", description = "Steward created",
                        content = {
                                @Content(mediaType = "application/json",
                                        schema = @Schema(implementation = StewardDto.class)
                                )})
        }
    )
    @PostMapping
    public ResponseEntity<StewardDto> createSteward(@RequestBody CreateStewardDto stewardToSave) {
        return new ResponseEntity<>(stewardFacade.createSteward(stewardToSave), HttpStatus.CREATED);
    }

    @Operation(
            summary = "Get specific steward by ID",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Steward found",
                            content = {
                                @Content(mediaType = "application/json",
                                        schema = @Schema(implementation = StewardDto.class)
                                )}),
                    @ApiResponse(responseCode = "404", description = "Steward with given ID was not found",
                            content = {
                                @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = RestExceptionResponse.class)
                                )})
            }
    )
    @Parameter(description = "ID of searched steward", in = ParameterIn.PATH, name = "id",
            schema = @Schema(type = "integer", format = "int64"))
    @GetMapping("/{id}")
    public ResponseEntity<StewardDto> findById(@PathVariable("id") Long id) {
        return new ResponseEntity<>(stewardFacade.findById(id), HttpStatus.OK);
    }


    @Operation(
            summary = "List all stewards",
            description = "Returns a list of all stewards. If there are no stewards it returns empty list.",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Stewards found",
                            content = {
                                @Content(mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = StewardDto.class))
                                )})
            }
    )
    @GetMapping
    public ResponseEntity<List<StewardDto>> getAllStewards() {
        return new ResponseEntity<>(stewardFacade.getAllStewards(), HttpStatus.OK);
    }

    @Operation(
            summary = "Update existing steward",
            description = "Accepts object representing steward with updated values of attributes and " +
                    "updates steward with corresponding ID if such steward exists in repository.",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Steward successfully updated. Returns updated steward.",
                            content = {
                                    @Content(mediaType = "application/json",
                                            schema = @Schema(implementation = StewardDto.class)
                                    )}),
                    @ApiResponse(responseCode = "404", description = "Steward with given ID was not found",
                            content = {
                                    @Content(mediaType = "application/json",
                                            schema = @Schema(implementation = RestExceptionResponse.class)
                                    )})
            }
    )
    @PutMapping
    public ResponseEntity<StewardDto> updateSteward(@RequestBody StewardDto stewardNewValues) {
        return new ResponseEntity<>(stewardFacade.updateSteward(stewardNewValues), HttpStatus.OK);
    }

    @Operation(
        summary = "delete steward by ID",
            responses = {
            @ApiResponse(responseCode = "200", description = "Steward successfully deleted. Returns deleted steward.",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = StewardDto.class)
                            )}),
            @ApiResponse(responseCode = "404", description = "Steward with given ID was not found",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = RestExceptionResponse.class)
                            )})
        }
    )
    @Parameter(description = "ID of steward to be deleted", in = ParameterIn.PATH, name = "id",
            schema = @Schema(type = "integer", format = "int64"))
    @DeleteMapping("/{id}")
    public ResponseEntity<StewardDto> deleteSteward(@PathVariable("id") Long id) {
        return new ResponseEntity<>(stewardFacade.deleteSteward(id), HttpStatus.OK);
    }

    @Operation(
            summary = "Assign steward to flight",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Some free steward was found for given flight and assigned to it",
                            content = {
                                    @Content(mediaType = "application/json",
                                            schema = @Schema(implementation = StewardDto.class)
                                    )}),
                    @ApiResponse(responseCode = "404", description = "No free steward for given flight was found",
                            content = {
                                    @Content(mediaType = "application/json",
                                            schema = @Schema(implementation = RestExceptionResponse.class)
                                    )})
            }
    )
    @Parameter(description = "ID flight to which we want to assign steward", in = ParameterIn.PATH, name = "flightId",
            schema = @Schema(type = "integer", format = "int64"))
    @GetMapping("/steward-to-flight/{flightId}")
    public ResponseEntity<StewardDto> assignStewardToFlight(@PathVariable("flightId") Long flightId) {
        return new ResponseEntity<>(stewardFacade.assignStewardToFlight(flightId), HttpStatus.OK);
    }
}
