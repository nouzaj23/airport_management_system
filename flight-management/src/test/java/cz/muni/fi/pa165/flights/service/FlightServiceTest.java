package cz.muni.fi.pa165.flights.service;

import cz.muni.fi.pa165.flights.exceptions.ResourceNotFoundException;
import cz.muni.fi.pa165.flights.model.Flight;
import cz.muni.fi.pa165.flights.data.repository.FlightRepository;
import cz.muni.fi.pa165.flights.util.TestDataFactory;
import cz.muni.fi.pa165.generated.model.FlightDTO;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class FlightServiceTest {

    @Mock
    private FlightRepository flightRepository;

    @Mock
    private ExternalServiceClient externalServiceClient;

    @InjectMocks
    private FlightService flightService;


    @Test
    void listFlights_repositoryReturnsFlights_returnsFlights() {
        List<Flight> flights = Arrays.asList(new Flight(), new Flight());
        when(flightRepository.listFlights()).thenReturn(flights);

        List<Flight> result = flightService.listFlights();

        assertEquals(flights, result);
        verify(flightRepository, times(1)).listFlights();
    }

    @Test
    void findById_flightExists_returnsFlight() {
        Long id = 1L;
        Flight flight = new Flight();
        when(flightRepository.findById(id)).thenReturn(Optional.of(flight));

        Flight result = flightService.findById(id);

        assertEquals(flight, result);
        verify(flightRepository, times(1)).findById(id);
    }

    @Test
    void findById_flightDoesNotExist_throwsResourceNotFoundException() {
        Long id = 1L;
        when(flightRepository.findById(id)).thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class, () -> flightService.findById(id));
        verify(flightRepository, times(1)).findById(id);
    }

    @Test
    void createFlight_validFlightDTO_createsFlight() {
        FlightDTO flightDTO = TestDataFactory.flightDTO;
        Flight flight = new Flight();
        when(flightRepository.createFlight(flightDTO)).thenReturn(flight);
        when(externalServiceClient.airplaneExists(any())).thenReturn(true);
        when(externalServiceClient.destinationExists(any())).thenReturn(true);

        Flight result = flightService.createFlight(flightDTO);

        assertEquals(flight, result);
        verify(flightRepository, times(1)).createFlight(flightDTO);
    }

    @Test
    void updateFlight_validFlightDTO_updatesFlight() {
        FlightDTO flightDTO = TestDataFactory.flightDTO;
        Flight flight = TestDataFactory.flightEntity;
        when(flightRepository.updateFlight(flightDTO)).thenReturn(Optional.of(flight));
        when(externalServiceClient.airplaneExists(any())).thenReturn(true);
        when(externalServiceClient.destinationExists(any())).thenReturn(true);

        Flight result = flightService.updateFlight(flightDTO);

        assertEquals(flight, result);
        verify(flightRepository, times(1)).updateFlight(flightDTO);
    }

    @Test
    void updateFlight_flightDoesNotExist_throwsResourceNotFoundException() {
        FlightDTO flightDTO = TestDataFactory.flightDTO;
        when(flightRepository.updateFlight(flightDTO)).thenReturn(Optional.empty());
        when(externalServiceClient.airplaneExists(any())).thenReturn(true);
        when(externalServiceClient.destinationExists(any())).thenReturn(true);

        assertThrows(ResourceNotFoundException.class, () -> flightService.updateFlight(flightDTO));
        verify(flightRepository, times(1)).updateFlight(flightDTO);
    }

    @Test
    void deleteFlight_flightExists_deletesFlight() {
        Long id = 1L;
        doNothing().when(flightRepository).deleteFlight(id);

        flightService.deleteFlight(id);

        verify(flightRepository, times(1)).deleteFlight(id);
    }

    @Test
    void addSteward_flightExists_addsSteward() {
        Long stewardId = 1L;
        Long flightId = 2L;
        Flight flight = new Flight();
        when(flightRepository.addSteward(stewardId, flightId)).thenReturn(Optional.of(flight));

        Flight result = flightService.addSteward(stewardId, flightId);

        assertEquals(flight, result);
        verify(flightRepository, times(1)).addSteward(stewardId, flightId);
    }

    @Test
    void addSteward_flightDoesNotExist_throwsResourceNotFoundException() {
        Long stewardId = 1L;
        Long flightId = 2L;
        when(flightRepository.addSteward(stewardId, flightId)).thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class, () -> flightService.addSteward(stewardId, flightId));
        verify(flightRepository, times(1)).addSteward(stewardId, flightId);
    }

    @Test
    void removeSteward_flightExists_removesSteward() {
        Long stewardId = 1L;
        Long flightId = 2L;
        Flight flight = new Flight();
        when(flightRepository.removeSteward(stewardId, flightId)).thenReturn(Optional.of(flight));

        Flight result = flightService.removeSteward(stewardId, flightId);

        assertEquals(flight, result);
        verify(flightRepository, times(1)).removeSteward(stewardId, flightId);
    }

    @Test
    void removeSteward_flightDoesNotExist_throwsResourceNotFoundException() {
        Long stewardId = 1L;
        Long flightId = 2L;
        when(flightRepository.removeSteward(stewardId, flightId)).thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class, () -> flightService.removeSteward(stewardId, flightId));
        verify(flightRepository, times(1)).removeSteward(stewardId, flightId);
    }
}
