package cz.muni.fi.pa165.flights.facade;

import cz.muni.fi.pa165.flights.service.FlightService;
import cz.muni.fi.pa165.generated.model.FlightDTO;
import cz.muni.fi.pa165.flights.mappers.FlightMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FlightFacade {

    private final FlightService flightService;
    private final FlightMapper flightMapper;

    @Autowired
    public FlightFacade(FlightService flightService, FlightMapper flightMapper) {
        this.flightService = flightService;
        this.flightMapper = flightMapper;
    }

    public List<FlightDTO> listFlights() {
        return flightMapper.mapToList(flightService.listFlights());
    }

    public FlightDTO findById(Long id) {
        return flightMapper.mapToDto(flightService.findById(id));
    }

    public FlightDTO createFlight(FlightDTO flightDTO) {
        return flightMapper.mapToDto(flightService.createFlight(flightDTO));
    }

    public FlightDTO updateFlight(FlightDTO flightDTO) {
        return flightMapper.mapToDto(flightService.updateFlight(flightDTO));
    }

    public void deleteFlight(Long id) {
        flightService.deleteFlight(id);
    }

    public FlightDTO addSteward(Long stewardId, Long flightId) {
        return flightMapper.mapToDto(flightService.addSteward(stewardId, flightId));
    }

    public FlightDTO removeSteward(Long stewardId, Long flightId) {
        return flightMapper.mapToDto(flightService.removeSteward(stewardId, flightId));
    }

}
