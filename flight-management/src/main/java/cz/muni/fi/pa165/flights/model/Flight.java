package cz.muni.fi.pa165.flights.model;

import java.time.OffsetDateTime;
import java.util.List;

public class Flight {

    private Long id;

    private OffsetDateTime departureTime;

    private OffsetDateTime arrivalTime;

    private Long originDestinationId;

    private Long finalDestinationId;

    private Long planeId;

    private List<Long> stewardIds;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public OffsetDateTime getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(OffsetDateTime departureTime) {
        this.departureTime = departureTime;
    }

    public OffsetDateTime getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(OffsetDateTime arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public Long getOriginDestinationId() {
        return originDestinationId;
    }

    public void setOriginDestinationId(Long originDestinationId) {
        this.originDestinationId = originDestinationId;
    }

    public Long getFinalDestinationId() {
        return finalDestinationId;
    }

    public void setFinalDestinationId(Long finalDestinationId) {
        this.finalDestinationId = finalDestinationId;
    }

    public Long getPlaneId() {
        return planeId;
    }

    public void setPlaneId(Long planeId) {
        this.planeId = planeId;
    }

    public List<Long> getStewardIds() {
        return stewardIds;
    }

    public void setStewardIds(List<Long> stewardIds) {
        this.stewardIds = stewardIds;
    }

    @Override
    public String toString() {
        return "Flight {" +
        "id=" + id +
        ", departureTime='" + departureTime +
        ", arrivalTime=" + arrivalTime +
        ", originDestination=" + originDestinationId +
        ", finalDestination=" + finalDestinationId +
        ", plane=" + planeId +
        ", stewards=" + stewardIds +
        "}";
    }
}
