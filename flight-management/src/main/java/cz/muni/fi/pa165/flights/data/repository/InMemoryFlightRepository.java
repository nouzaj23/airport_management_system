package cz.muni.fi.pa165.flights.data.repository;

import cz.muni.fi.pa165.flights.model.Flight;
import cz.muni.fi.pa165.generated.model.FlightDTO;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public class InMemoryFlightRepository implements FlightRepository {

    private final List<Flight> flights = new ArrayList<>();
    private Long id = 1L;

    @Override
    public List<Flight> listFlights() {
        return flights;
    }

    @Override
    public Optional<Flight> findById(Long id) {
        return flights
                .stream()
                .filter(flight -> flight.getId().equals(id))
                .findFirst();
    }

    @Override
    public Flight createFlight(FlightDTO flightDTO) {
        Flight flight = new Flight();
        flight.setId(id++);
        flight.setDepartureTime(flightDTO.getDepartureTime());
        flight.setArrivalTime(flightDTO.getArrivalTime());
        flight.setOriginDestinationId(flightDTO.getOriginDestinationId());
        flight.setFinalDestinationId(flightDTO.getFinalDestinationId());
        flight.setPlaneId(flightDTO.getPlaneId());
        flight.setStewardIds(flightDTO.getStewardIds());
        flights.add(flight);
        return flight;
    }

    @Override
    public Optional<Flight> updateFlight(FlightDTO flightDTO) {
        Optional<Flight> optionalFlight = findById(flightDTO.getId());
        if (optionalFlight.isPresent()) {
            Flight flight = optionalFlight.get();
            flight.setDepartureTime(flightDTO.getDepartureTime());
            flight.setArrivalTime(flightDTO.getArrivalTime());
            flight.setOriginDestinationId(flightDTO.getOriginDestinationId());
            flight.setFinalDestinationId(flightDTO.getFinalDestinationId());
            flight.setPlaneId(flightDTO.getPlaneId());
            flight.setStewardIds(flightDTO.getStewardIds());
            return Optional.of(flight);
        } else {
            return Optional.empty();
        }
    }

    @Override
    public void deleteFlight(Long id) {
        flights.removeIf(flight -> flight.getId().equals(id));
    }

    @Override
    public Optional<Flight> addSteward(Long stewardId, Long flightId) {
        Optional<Flight> optionalFlight = findById(flightId);
        if (optionalFlight.isPresent()) {
            Flight flight = optionalFlight.get();
            List<Long> stewardIds = flight.getStewardIds();
            if (stewardIds.stream().filter(id -> id.equals(stewardId)).findAny().isEmpty()) {
                stewardIds.add(stewardId);
                flight.setStewardIds(stewardIds);
            }
            return Optional.of(flight);
        }
        return Optional.empty();
    }

    @Override
    public Optional<Flight> removeSteward(Long stewardId, Long flightId) {
        Optional<Flight> optionalFlight = findById(flightId);
        if (optionalFlight.isPresent()) {
            Flight flight = optionalFlight.get();
            List<Long> stewardIds = flight.getStewardIds();
            stewardIds.remove(stewardId);
            flight.setStewardIds(stewardIds);
            return Optional.of(flight);
        }
        return Optional.empty();
    }


}
