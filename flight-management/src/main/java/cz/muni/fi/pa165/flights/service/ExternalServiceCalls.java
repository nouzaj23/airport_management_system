package cz.muni.fi.pa165.flights.service;

import cz.muni.fi.pa165.flights.model.Airplane;
import cz.muni.fi.pa165.flights.model.Destination;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClient;

@Service
public class ExternalServiceCalls implements ExternalServiceClient {

    private static final String ADDRESS = "http://localhost:8082/";
    private static final String AIRPLANE_PATH = "airplanes";
    private static final String DESTINATION_PATH = "destinations";

    public boolean airplaneExists(Long id) {
        String uri = ADDRESS + AIRPLANE_PATH + '/' + id;
        RestClient restClient = RestClient.create();
        ResponseEntity<Airplane> airplaneResponseEntity = restClient.get()
                .uri(uri)
                .retrieve()
                .toEntity(Airplane.class);
        return airplaneResponseEntity.getStatusCode() == HttpStatus.OK;
    }

    public boolean destinationExists(Long id) {
        String uri = ADDRESS + DESTINATION_PATH + '/' + id;
        RestClient restClient = RestClient.create();
        ResponseEntity<Destination> destinationResponseEntity = restClient.get()
                .uri(uri)
                .retrieve()
                .toEntity(Destination.class);
        return destinationResponseEntity.getStatusCode() == HttpStatus.OK;
    }

}
