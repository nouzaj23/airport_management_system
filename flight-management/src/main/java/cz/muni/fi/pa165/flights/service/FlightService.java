package cz.muni.fi.pa165.flights.service;

import cz.muni.fi.pa165.flights.model.Flight;
import cz.muni.fi.pa165.flights.data.repository.FlightRepository;
import cz.muni.fi.pa165.flights.exceptions.DataStorageException;
import cz.muni.fi.pa165.flights.exceptions.ResourceNotFoundException;
import cz.muni.fi.pa165.generated.model.FlightDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FlightService {

    private final FlightRepository flightRepository;
    private final ExternalServiceClient externalServiceClient;

    @Autowired
    public FlightService(FlightRepository flightRepository, ExternalServiceClient externalServiceClient) {
        this.flightRepository = flightRepository;
        this.externalServiceClient = externalServiceClient;
    }

    public List<Flight> listFlights() {
        return flightRepository.listFlights();
    }

    public Flight findById(Long id) {
        return flightRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Flight with id: " + id + " was not found"));
    }

    public Flight createFlight(FlightDTO flightDTO) {
        verifyParameters(flightDTO);
        return flightRepository.createFlight(flightDTO);
    }

    public Flight updateFlight(FlightDTO flightDTO) {
        verifyParameters(flightDTO);
        return flightRepository.updateFlight(flightDTO)
                .orElseThrow(() -> new ResourceNotFoundException("Airplane with id: " + flightDTO.getId() + " was not found"));
    }

    public void deleteFlight(Long id) {
        flightRepository.deleteFlight(id);
    }

    public Flight addSteward(Long stewardId, Long flightId) {
        return flightRepository.addSteward(stewardId, flightId)
                .orElseThrow(() -> new ResourceNotFoundException("Airplane with id: " + flightId + " was not found"));
    }

    public Flight removeSteward(Long stewardId, Long flightId) {
        return flightRepository.removeSteward(stewardId, flightId)
                .orElseThrow(() -> new ResourceNotFoundException("Airplane with id: " + flightId + " was not found"));
    }

    private void verifyParameters(FlightDTO flightDTO) {

        Long planeId = flightDTO.getPlaneId();
        if (! externalServiceClient.airplaneExists(planeId)) {
            throw new DataStorageException("Airplane with id: " + planeId + " does not exist");
        }

        Long originDestId = flightDTO.getOriginDestinationId();
        if (! externalServiceClient.destinationExists(originDestId)) {
            throw new DataStorageException("Destination with id: " + originDestId + " does not exist");
        }

        Long finalDestId = flightDTO.getFinalDestinationId();
        if (! externalServiceClient.destinationExists(finalDestId)) {
            throw new DataStorageException("Destination with id: " + finalDestId + " does not exist");
        }

        if (originDestId.equals(finalDestId)) {
            throw new DataStorageException("Final destination cannot be same as starting destination");
        }
    }

}
