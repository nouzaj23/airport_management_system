package cz.muni.fi.pa165.flights.service;

public interface ExternalServiceClient {

    boolean airplaneExists(Long id);

    boolean destinationExists(Long id);

}
