package cz.muni.fi.pa165.flights.data.repository;

import cz.muni.fi.pa165.flights.model.Flight;
import cz.muni.fi.pa165.generated.model.FlightDTO;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface FlightRepository {

    List<Flight> listFlights();

    Optional<Flight> findById(Long id);

    Flight createFlight(FlightDTO flightDTO);

    Optional<Flight> updateFlight(FlightDTO flightDTO);

    void deleteFlight(Long id);

    Optional<Flight> addSteward(Long stewardId, Long flightId);

    Optional<Flight> removeSteward(Long stewardId, Long flightId);

}
