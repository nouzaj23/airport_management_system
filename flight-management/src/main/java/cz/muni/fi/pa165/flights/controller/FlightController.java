package cz.muni.fi.pa165.flights.controller;

import cz.muni.fi.pa165.flights.facade.FlightFacade;
import cz.muni.fi.pa165.generated.api.FlightsApiDelegate;
import cz.muni.fi.pa165.generated.model.FlightDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "/flights")
public class FlightController implements FlightsApiDelegate {

    private final FlightFacade flightFacade;

    @Autowired
    public FlightController(FlightFacade flightFacade) {
        this.flightFacade = flightFacade;
    }

    @Override
    @PostMapping
    public ResponseEntity<FlightDTO> createFlight(@RequestBody FlightDTO flightDTO) {
        return ResponseEntity.ok(flightFacade.createFlight(flightDTO));
    }

    @Override
    @GetMapping
    public ResponseEntity<List<FlightDTO>> listFlights() {
        return ResponseEntity.ok(flightFacade.listFlights());
    }

    @Override
    @GetMapping(path = "/{id}")
    public ResponseEntity<FlightDTO> getFlight(@PathVariable("id") Long id) {
        return ResponseEntity.ok(flightFacade.findById(id));
    }

    @Override
    @PutMapping(path = "/{id}")
    public ResponseEntity<FlightDTO> updateFlight(@PathVariable("id") Long id, @RequestBody FlightDTO flightDTO) {
        return ResponseEntity.ok(flightFacade.updateFlight(flightDTO.id(id)));
    }

    @Override
    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Void> deleteFlight(@PathVariable("id") Long id) {
        flightFacade.deleteFlight(id);
        return ResponseEntity.noContent().build();
    }

    @Override
    @PutMapping(path = "/steward")
    public ResponseEntity<FlightDTO> addSteward(@RequestParam Long stewardId, @RequestParam Long flightId) {
        return ResponseEntity.ok(flightFacade.addSteward(stewardId, flightId));
    }

    @Override
    @DeleteMapping(path = "/steward")
    public ResponseEntity<FlightDTO> removeSteward(@RequestParam Long stewardId, @RequestParam Long flightId) {
        return ResponseEntity.ok(flightFacade.removeSteward(stewardId, flightId));
    }
}
