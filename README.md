# Airport Management System

Our project aims to develop an information system for managing flight records at an airport. This system facilitates the entry, update, and deletion of records pertaining to stewards, airplanes, and destinations. It allows for the recording of flights, ensuring validity by checking airplane availability and steward assignment. Airport managers can utilize features such as CRUD operations for flights, stewards, airplanes, and destinations. Customer service functionalities include filtering and ordering flights based on various criteria.

### Data classes

- Destination: airport location (country, city)
- Airplane: capacity of the plane, its name and optionally its type
- Steward: first name, last name
- Flight: departure and arrival times, the origin, the destination and the plane

### Services

1. Flight management service 
    - CRUD operations for Flights
    - Scheduling Flights
    - Listing flights
    - Checking Airplane availability

2. Steward management service
    - CRUD operations for Stewards
    - Managing schedule and availability of Stewards

3. Resource management service
    - CRUD operations for Airplanes and Destinations

### Use Case
![Airport management system UC diagram](UseCase.png)

### Class Diagram
![Airport management system class diagram](ClassDiagram.png)