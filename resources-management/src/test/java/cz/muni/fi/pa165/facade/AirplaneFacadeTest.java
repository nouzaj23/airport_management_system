package cz.muni.fi.pa165.facade;

import cz.muni.fi.pa165.generated.model.AirplaneDTO;
import cz.muni.fi.pa165.resources.exceptions.ResourceNotFoundException;
import cz.muni.fi.pa165.resources.facade.AirplaneFacade;
import cz.muni.fi.pa165.resources.mappers.AirplaneMapper;
import cz.muni.fi.pa165.resources.model.Airplane;
import cz.muni.fi.pa165.resources.service.AirplaneService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class AirplaneFacadeTest {
    @Mock
    private AirplaneService airplaneService;

    @Mock
    private AirplaneMapper airplaneMapper;

    @InjectMocks
    private AirplaneFacade airplaneFacade;

    private Airplane createAirplane() {
        Airplane airplane = new Airplane();
        airplane.setName("AirpName1");
        airplane.setType("AirplType1");
        airplane.setCapacity(100);
        return airplane;
    }

    private AirplaneDTO createAirplaneDto() {
        AirplaneDTO airplaneDTO = new AirplaneDTO();
        airplaneDTO.setName("AirpName1");
        airplaneDTO.setType("AirplType1");
        airplaneDTO.setCapacity(100);
        return airplaneDTO;
    }

    @Test
    void listAirplanes_noAirplanesListed_returnsEmptyList() {
        when(airplaneService.listAirplanes()).thenReturn(new ArrayList<>());
        when(airplaneMapper.mapToList(anyList())).thenReturn(new ArrayList<>());

        List<AirplaneDTO> result = airplaneFacade.listAirplanes();

        assertThat(result).isEmpty();
    }


    @Test
    void listAirplanes_AirplanesExist_returnsMappedAirplanes() {
        Airplane airplane1 = createAirplane();
        Airplane airplane2 = createAirplane();
        airplane2.setName("AirpName2");
        airplane2.setType("AirplType2");
        airplane2.setCapacity(200);

        AirplaneDTO airplaneDTO1 = createAirplaneDto();
        AirplaneDTO airplaneDTO2 = createAirplaneDto();
        airplaneDTO2.setName("AirpName2");
        airplaneDTO2.setType("AirplType2");
        airplaneDTO2.setCapacity(200);

        List<Airplane> airplanes = List.of(airplane1, airplane2);
        List<AirplaneDTO> airplaneDTOS = List.of(airplaneDTO1, airplaneDTO2);
        when(airplaneService.listAirplanes()).thenReturn(airplanes);
        when(airplaneMapper.mapToList(airplanes)).thenReturn(airplaneDTOS);

        List<AirplaneDTO> result = airplaneFacade.listAirplanes();

        assertThat(result).isEqualTo(airplaneDTOS);
        verify(airplaneService).listAirplanes();
        verify(airplaneMapper).mapToList(anyList());
    }

    @Test
    void findById_airplaneExists_returnsMappedAirplane() {
        AirplaneDTO airplaneDTO = createAirplaneDto();
        Airplane airplane = createAirplane();

        when(airplaneService.findById(1L)).thenReturn(airplane);
        when(airplaneMapper.mapToDto(airplane)).thenReturn(airplaneDTO);

        AirplaneDTO result = airplaneFacade.findById(1L);

        assertThat(result).isEqualTo(airplaneDTO);
    }

    @Test
    void findById_airplaneDoesNotExist_throwsResourceNotFoundException() {
        when(airplaneService.findById(1L)).thenThrow(
                new ResourceNotFoundException("Airplane with id: 1L was not found."));

        assertThrows(ResourceNotFoundException.class, () -> airplaneFacade.findById(1L));
        verify(airplaneService).findById(anyLong());
    }

    @Test
    void createAirplane_successful_returnsMappedAirplane() {
        AirplaneDTO airplaneDTO = createAirplaneDto();
        Airplane airplane = createAirplane();

        when(airplaneService.createAirplane(airplaneDTO)).thenReturn(airplane);
        when(airplaneMapper.mapToDto(airplane)).thenReturn(airplaneDTO);

        AirplaneDTO result = airplaneFacade.createAirplane(airplaneDTO);

        assertThat(result).isEqualTo(airplaneDTO);
        verify(airplaneService).createAirplane(airplaneDTO);
        verify(airplaneMapper).mapToDto(airplane);
    }

    @Test
    void updateAirplane_airplaneExists_returnsMappedAirplane() {
        AirplaneDTO airplaneDTO = createAirplaneDto();
        Airplane airplane = createAirplane();
        when(airplaneService.updateAirplane(airplaneDTO)).thenReturn(airplane);
        when(airplaneMapper.mapToDto(airplane)).thenReturn(airplaneDTO);

        AirplaneDTO result = airplaneFacade.updateAirplane(airplaneDTO);

        assertThat(result).isEqualTo(airplaneDTO);
        verify(airplaneService).updateAirplane(airplaneDTO);
        verify(airplaneMapper).mapToDto(airplane);
    }

    @Test
    void updateAirplane_serviceThrowsResourceNotFoundException_throwsRuntimeException() {
        AirplaneDTO airplaneDTO = createAirplaneDto();
        when(airplaneService.updateAirplane(airplaneDTO))
                .thenThrow(new ResourceNotFoundException("Airplane with id: " + 1L + " was not found"));

        assertThrows(RuntimeException.class, () -> airplaneFacade.updateAirplane(airplaneDTO));
        verify(airplaneService).updateAirplane(airplaneDTO);
    }

    @Test
    void deleteAirplane_successful_callsDeleteAirplane() {
        doNothing().when(airplaneService).deleteAirplane(1L);

        airplaneFacade.deleteAirplane(1L);

        verify(airplaneService).deleteAirplane(anyLong());
    }
}
