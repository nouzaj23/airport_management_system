package cz.muni.fi.pa165.service;

import cz.muni.fi.pa165.generated.model.AirplaneDTO;
import cz.muni.fi.pa165.resources.data.repository.AirplaneRepository;
import cz.muni.fi.pa165.resources.exceptions.ResourceNotFoundException;
import cz.muni.fi.pa165.resources.model.Airplane;
import cz.muni.fi.pa165.resources.service.AirplaneService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class AirplaneServiceTest {
    @Mock
    private AirplaneRepository airplaneRepository;

    @InjectMocks
    private AirplaneService airplaneService;

    private Airplane createAirplane() {
        Airplane airplane = new Airplane();
        airplane.setName("AirpName1");
        airplane.setType("AirplType1");
        airplane.setCapacity(100);
        return airplane;
    }

    private AirplaneDTO createAirplaneDto() {
        AirplaneDTO airplaneDTO = new AirplaneDTO();
        airplaneDTO.setName("AirpName1");
        airplaneDTO.setType("AirplType1");
        airplaneDTO.setCapacity(100);
        return airplaneDTO;
    }

    @Test
    void createAirplane_creationSuccessful_returnsAirplane() {
        // Arrange
        Airplane airplane = createAirplane();
        AirplaneDTO airplaneDTO = createAirplaneDto();
        when(airplaneRepository.createAirplane(airplaneDTO)).thenReturn(airplane);

        // Act
        Airplane returnedAirplane = airplaneService.createAirplane(airplaneDTO);

        // Assert
        assertThat(returnedAirplane).isEqualTo(airplane);
    }

    @Test
    void findById_airplaneFound_returnsAirplane() {
        // Arrange
        Airplane airplane = createAirplane();
        AirplaneDTO airplaneDTO = createAirplaneDto();
        when(airplaneRepository.findById(1L)).thenReturn(Optional.of(airplane));

        // Act
        Airplane returnedAirplane = airplaneService.findById(1L);

        // Assert
        assertThat(returnedAirplane).isEqualTo(airplane);
    }

    @Test
    void findById_airplaneNotFound_throwsException() {
        // Arrange
        when(airplaneRepository.findById(1L)).thenReturn(Optional.empty());

        // Act
        assertThrows(ResourceNotFoundException.class, () -> airplaneService.findById(1L));

        // Assert
        Mockito.verify(airplaneRepository, times(1)).findById(1L);
    }

    @Test
    void getAllAirplanes_noAirplanesListed_returnsEmptyList() {
        // Arrange
        when(airplaneRepository.listAirplanes()).thenReturn(new ArrayList<>());

        // Act
        List<Airplane> returnedAirplane = airplaneService.listAirplanes();

        // Assert
        assertThat(returnedAirplane).isEmpty();
    }

    @Test
    void getAllAirplanes_someAirplanesListed_returnsListOfAirplanes() {
        // Arrange
        Airplane airplane1 = createAirplane();
        Airplane airplane2 = createAirplane();
        airplane2.setName("AirpName2");
        airplane2.setType("AirplType2");
        airplane2.setCapacity(200);

        List<Airplane> airplanes = List.of(airplane1, airplane2);
        when(airplaneRepository.listAirplanes()).thenReturn(airplanes);

        // Act
        List<Airplane> returnedAirplane = airplaneService.listAirplanes();

        // Assert
        assertThat(returnedAirplane).isEqualTo(airplanes);
    }

    @Test
    void updateAirplane_airplaneFound_returnsAirplane() {
        // Arrange
        Airplane airplane = createAirplane();
        AirplaneDTO airplaneDTO = createAirplaneDto();
        when(airplaneRepository.updateAirplane(airplaneDTO)).thenReturn(Optional.of(airplane));

        // Act
        Airplane returnedAirplane = airplaneService.updateAirplane(airplaneDTO);

        // Assert
        assertThat(returnedAirplane).isEqualTo(airplane);
    }

    @Test
    void updateDestination_destinationNotFound_throwsException() {
        // Arrange
        AirplaneDTO airplaneDTO = createAirplaneDto();
        when(airplaneRepository.updateAirplane(airplaneDTO)).thenReturn(Optional.empty());

        // Act
        assertThrows(ResourceNotFoundException.class, () -> airplaneService.updateAirplane(airplaneDTO));

        // Assert
        Mockito.verify(airplaneRepository, times(1)).updateAirplane(airplaneDTO);
    }


    @Test
    void deleteDestination_destinationFound_returnsVoid() {
        // Arrange
        Long id = 1L;
        doNothing().when(airplaneRepository).deleteAirplane(id);

        // Act
        airplaneService.deleteAirplane(id);

        // Assert
        verify(airplaneRepository, times(1)).deleteAirplane(id);
    }
}
