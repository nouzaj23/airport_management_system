package cz.muni.fi.pa165.service;

import cz.muni.fi.pa165.generated.model.DestinationDTO;
import cz.muni.fi.pa165.resources.data.repository.DestinationRepository;
import cz.muni.fi.pa165.resources.exceptions.ResourceNotFoundException;
import cz.muni.fi.pa165.resources.model.Destination;
import cz.muni.fi.pa165.resources.service.DestinationService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class DestinationServiceTest {
    @Mock
    private DestinationRepository destinationRepository;

    @InjectMocks
    private DestinationService destinationService;

    private Destination createDestination() {
        Destination destination = new Destination();
        destination.setCountry("CountryName1");
        destination.setCity("CityName1");
        destination.setAirportName("airpName1");
        return destination;
    }

    private DestinationDTO createDtoDestination() {
        DestinationDTO destinationDto = new DestinationDTO();
        destinationDto.setCountry("CountryName1");
        destinationDto.setCity("CityName1");
        destinationDto.setAirportName("airpName1");
        return destinationDto;
    }

    @Test
    void createDestination_creationSuccessful_returnsDestination() {
        // Arrange
        Destination destination = createDestination();
        DestinationDTO destinationDto = createDtoDestination();
        when(destinationRepository.createDestination(destinationDto)).thenReturn(destination);

        // Act
        Destination returnedDestination = destinationService.createDestination(destinationDto);

        // Assert
        assertThat(returnedDestination).isEqualTo(destination);
    }

    @Test
    void findById_destinationFound_returnsDestination() {
        // Arrange
        Destination destination = createDestination();
        DestinationDTO destinationDto = createDtoDestination();
        when(destinationRepository.findById(1L)).thenReturn(Optional.of(destination));

        // Act
        Destination returnedDestination = destinationService.findById(1L);

        // Assert
        assertThat(returnedDestination).isEqualTo(destination);
    }

    @Test
    void findById_destinationNotFound_throwsException() {
        // Arrange
        when(destinationRepository.findById(1L)).thenReturn(Optional.empty());

        // Act
        assertThrows(ResourceNotFoundException.class, () -> destinationService.findById(1L));

        // Assert
        Mockito.verify(destinationRepository, times(1)).findById(1L);
    }

    @Test
    void getAllDestinations_noDestinationsListed_returnsEmptyList() {
        // Arrange
        when(destinationRepository.listDestinations()).thenReturn(new ArrayList<>());

        // Act
        List<Destination> returnedDestinations = destinationService.listDestinations();

        // Assert
        assertThat(returnedDestinations).isEmpty();
    }

    @Test
    void getAllDestinations_someDestinationsListed_returnsListOfDestinations() {
        // Arrange
        Destination destination1 = createDestination();

        Destination destination2 = createDestination();
        destination2.setCountry("CountryName2");
        destination2.setCity("CityName2");
        destination2.setAirportName("airpName2");

        List<Destination> destinations = List.of(destination1, destination2);
        when(destinationRepository.listDestinations()).thenReturn(destinations);

        // Act
        List<Destination> returnedDestinations = destinationService.listDestinations();

        // Assert
        assertThat(returnedDestinations).isEqualTo(destinations);
    }

    @Test
    void updateDestination_destinationFound_returnsDestination() {
        // Arrange
        Destination destination = createDestination();
        DestinationDTO destinationDto = createDtoDestination();
        when(destinationRepository.updateDestination(destinationDto)).thenReturn(Optional.of(destination));

        // Act
        Destination returnedDestination = destinationService.updateDestination(destinationDto);

        // Assert
        assertThat(returnedDestination).isEqualTo(destination);
    }

    @Test
    void updateDestination_destinationNotFound_throwsException() {
        // Arrange
        DestinationDTO destinationDto = createDtoDestination();
        when(destinationRepository.updateDestination(destinationDto)).thenReturn(Optional.empty());

        // Act
        assertThrows(ResourceNotFoundException.class, () -> destinationService.updateDestination(destinationDto));

        // Assert
        Mockito.verify(destinationRepository, times(1)).updateDestination(destinationDto);
    }


    @Test
    void deleteDestination_destinationFound_returnsVoid() {
        // Arrange
        Long id = 1L;
        doNothing().when(destinationRepository).deleteDestination(id);

        // Act
        destinationService.deleteDestination(id);

        // Assert
        verify(destinationRepository, times(1)).deleteDestination(id);
    }
}
