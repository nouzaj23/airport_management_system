package cz.muni.fi.pa165.resources.service;

import cz.muni.fi.pa165.generated.model.AirplaneDTO;
import cz.muni.fi.pa165.resources.data.repository.AirplaneRepository;
import cz.muni.fi.pa165.resources.exceptions.ResourceNotFoundException;
import cz.muni.fi.pa165.resources.model.Airplane;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AirplaneService {

    private final AirplaneRepository airplaneRepository;

    @Autowired
    public AirplaneService(AirplaneRepository airplaneRepository) {
        this.airplaneRepository = airplaneRepository;
    }

    public List<Airplane> listAirplanes() {
        return airplaneRepository.listAirplanes();
    }

    public Airplane findById(Long id) {
        return airplaneRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Airplane with id: " + id + " was not found"));
    }

    public Airplane createAirplane(AirplaneDTO airplaneDTO) {
        return airplaneRepository.createAirplane(airplaneDTO);
    }

    public Airplane updateAirplane(AirplaneDTO airplaneDTO) {
        return airplaneRepository.updateAirplane(airplaneDTO)
                .orElseThrow(() -> new ResourceNotFoundException("Airplane with id: " + airplaneDTO.getId() + " was not found"));
    }

    public void deleteAirplane(Long id) {
        airplaneRepository.deleteAirplane(id);
    }
}
