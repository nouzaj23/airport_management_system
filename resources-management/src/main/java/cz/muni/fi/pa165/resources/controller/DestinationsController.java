package cz.muni.fi.pa165.resources.controller;

import cz.muni.fi.pa165.generated.api.DestinationsApiDelegate;
import cz.muni.fi.pa165.generated.model.DestinationDTO;
import cz.muni.fi.pa165.resources.facade.DestinationFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Service
@RestController
@RequestMapping(path = "/destinations")
public class DestinationsController implements DestinationsApiDelegate {

    private final DestinationFacade destinationFacade;

    @Autowired
    public DestinationsController(DestinationFacade destinationFacade) {
        this.destinationFacade = destinationFacade;
    }

    @Override
    @PostMapping
    public ResponseEntity<DestinationDTO> createDestination(@RequestBody DestinationDTO destinationDTO) {
        return ResponseEntity.ok(destinationFacade.createDestination(destinationDTO));
    }

    @Override
    @GetMapping
    public ResponseEntity<List<DestinationDTO>> listDestinations() {
        return ResponseEntity.ok(destinationFacade.listDestinations());
    }

    @Override
    @GetMapping(path = "/{id}")
    public ResponseEntity<DestinationDTO> getDestination(@PathVariable("id") Long id) {
        return ResponseEntity.ok(destinationFacade.findById(id));
    }

    @Override
    @PutMapping(path = "/{id}")
    public ResponseEntity<DestinationDTO> updateDestination(@PathVariable("id") Long id, @RequestBody DestinationDTO destinationDTO) {
        return ResponseEntity.ok(destinationFacade.updateDestination(destinationDTO.id(id)));
    }

    @Override
    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Void> deleteDestination(@PathVariable("id") Long id) {
        destinationFacade.deleteDestination(id);
        return ResponseEntity.noContent().build();
    }
}
