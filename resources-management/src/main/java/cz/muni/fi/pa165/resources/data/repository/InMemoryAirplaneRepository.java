package cz.muni.fi.pa165.resources.data.repository;

import cz.muni.fi.pa165.generated.model.AirplaneDTO;
import cz.muni.fi.pa165.resources.model.Airplane;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Repository
public class InMemoryAirplaneRepository implements AirplaneRepository {
    private final List<Airplane> airplanes = new ArrayList<>();
    private Long id = 1L;

    @Override
    public List<Airplane> listAirplanes() {
        return airplanes;
    }

    @Override
    public Optional<Airplane> findById(Long id) {
        return airplanes
                .stream()
                .filter(airplane -> Objects.equals(airplane.getId(), id))
                .findFirst();
    }

    @Override
    public Airplane createAirplane(AirplaneDTO airplaneDTO) {
        Airplane airplane = new Airplane();
        airplane.setId(id++);
        airplane.setName(airplaneDTO.getName());
        airplane.setType(airplaneDTO.getType());
        airplane.setCapacity(airplaneDTO.getCapacity());
        airplanes.add(airplane);
        return airplane;
    }

    @Override
    public Optional<Airplane> updateAirplane(AirplaneDTO airplaneDTO) {
        Optional<Airplane> optionalAirplane = findById(Long.valueOf(airplaneDTO.getId()));
        if (optionalAirplane.isPresent()) {
            Airplane airplane = optionalAirplane.get();
            airplane.setName(airplaneDTO.getName());
            airplane.setType(airplaneDTO.getType());
            airplane.setCapacity(airplaneDTO.getCapacity());
            return Optional.of(airplane);
        } else {
            return Optional.empty();
        }
    }

    @Override
    public void deleteAirplane(Long id) {
        airplanes.removeIf(airplane -> Objects.equals(airplane.getId(), id));
    }
}
