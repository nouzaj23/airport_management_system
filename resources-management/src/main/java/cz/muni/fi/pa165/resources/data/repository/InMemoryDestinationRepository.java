package cz.muni.fi.pa165.resources.data.repository;

import cz.muni.fi.pa165.generated.model.DestinationDTO;
import cz.muni.fi.pa165.resources.model.Destination;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Repository
public class InMemoryDestinationRepository implements DestinationRepository {
    private final List<Destination> destinations = new ArrayList<>();
    private Long id = 1L;

    @Override
    public List<Destination> listDestinations() {
        return destinations;
    }

    @Override
    public Optional<Destination> findById(Long id) {
        return destinations
                .stream()
                .filter(destination -> Objects.equals(destination.getId(), id))
                .findFirst();
    }

    @Override
    public Destination createDestination(DestinationDTO destinationDTO) {
        Destination destination = new Destination();
        destination.setId(id++);
        destination.setCountry(destinationDTO.getCountry());
        destination.setCity(destinationDTO.getCity());
        destination.setAirportName(destinationDTO.getAirportName());
        destinations.add(destination);
        return destination;
    }

    @Override
    public Optional<Destination> updateDestination(DestinationDTO destinationDTO) {
        Optional<Destination> optionalDestination = findById(Long.valueOf(destinationDTO.getId()));
        if (optionalDestination.isPresent()) {
            Destination destination = optionalDestination.get();
            destination.setCountry(destinationDTO.getCountry());
            destination.setCity(destinationDTO.getCity());
            destination.setAirportName(destinationDTO.getAirportName());
            return Optional.of(destination);
        } else {
            return Optional.empty();
        }
    }

    @Override
    public void deleteDestination(Long id) {
        destinations.removeIf(destination -> Objects.equals(destination.getId(), id));
    }
}
