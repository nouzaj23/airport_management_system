package cz.muni.fi.pa165.resources.service;

import cz.muni.fi.pa165.generated.model.DestinationDTO;
import cz.muni.fi.pa165.resources.data.repository.DestinationRepository;
import cz.muni.fi.pa165.resources.exceptions.ResourceNotFoundException;
import cz.muni.fi.pa165.resources.model.Destination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DestinationService {

    private final DestinationRepository destinationRepository;

    @Autowired
    public DestinationService(DestinationRepository destinationRepository) {
        this.destinationRepository = destinationRepository;
    }

    public List<Destination> listDestinations() {
        return destinationRepository.listDestinations();
    }

    public Destination findById(Long id) {
        return destinationRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Destination with id: " + id + " was not found."));
    }

    public Destination createDestination(DestinationDTO destinationDTO) {
        return destinationRepository.createDestination(destinationDTO);
    }

    public Destination updateDestination(DestinationDTO destinationDTO) {
        return destinationRepository.updateDestination(destinationDTO)
                .orElseThrow(() -> new ResourceNotFoundException("Destination with id: " + destinationDTO.getId() + " was not found."));
    }

    public void deleteDestination(Long id) {
        destinationRepository.deleteDestination(id);
    }
}
