package cz.muni.fi.pa165.resources.data.repository;

import cz.muni.fi.pa165.generated.model.DestinationDTO;
import cz.muni.fi.pa165.resources.model.Destination;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface DestinationRepository {

    List<Destination> listDestinations();

    Optional<Destination> findById(Long id);

    Destination createDestination(DestinationDTO destinationDTO);

    Optional<Destination> updateDestination(DestinationDTO destinationDTO);

    void deleteDestination(Long id);
}
