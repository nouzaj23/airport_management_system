package cz.muni.fi.pa165.resources.data.repository;

import cz.muni.fi.pa165.generated.model.AirplaneDTO;
import cz.muni.fi.pa165.resources.model.Airplane;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AirplaneRepository {

    List<Airplane> listAirplanes();

    Optional<Airplane> findById(Long id);

    Airplane createAirplane(AirplaneDTO airplaneDTO);

    Optional<Airplane> updateAirplane(AirplaneDTO airplaneDTO);

    void deleteAirplane(Long id);


}
