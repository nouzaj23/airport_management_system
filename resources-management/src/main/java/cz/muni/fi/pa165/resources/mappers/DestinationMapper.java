package cz.muni.fi.pa165.resources.mappers;

import cz.muni.fi.pa165.generated.model.DestinationDTO;
import cz.muni.fi.pa165.resources.model.Destination;
import org.mapstruct.Mapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.List;

@Mapper(componentModel = "spring")
public interface DestinationMapper {
    DestinationDTO mapToDto(Destination destination);

    List<DestinationDTO> mapToList(List<Destination> destinations);

    default Page<DestinationDTO> mapToPageDto(Page<Destination> destinations) {
        return new PageImpl<>(mapToList(destinations.getContent()), destinations.getPageable(), destinations.getTotalPages());
    }
}
